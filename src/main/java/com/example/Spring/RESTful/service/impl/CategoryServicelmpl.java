package com.example.Spring.RESTful.service.impl;

import com.example.Spring.RESTful.respository.BookRespository;
import com.example.Spring.RESTful.respository.CategoryRespository;
import com.example.Spring.RESTful.respository.dto.Category;
import com.example.Spring.RESTful.rest.response.Messages;
import com.example.Spring.RESTful.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServicelmpl implements CategoryService {

    private CategoryRespository categoryRespository;

    @Autowired
    public void setCategoryRespository(CategoryRespository categoryRespository) {
        this.categoryRespository = categoryRespository;
    }


    @Override
    public List<Category> getAllCategories() {
        return categoryRespository.getAllCategories();
    }

    @Override
    public Category postCategory(Category category) {
        if (categoryRespository.postCategory(category)){
            int id = Integer.parseInt(categoryRespository.getCategoryIdByTitle(category.getTitle()));
            category.setId(id);
            return category;
        }else {
            return null;
        }
    }

    @Override
    public String deleteCategory(int Id) {
        if (categoryRespository.deleteCategory(Id)){
            return "You have deleted a category successfully!";
        }else {
            return "You are failed to delete a category!";
        }
    }

    @Override
    public Category updateCategory(int Id, Category category) {
        if (categoryRespository.updateCategory(Id,category)){
            int idCategory = Integer.parseInt(categoryRespository.getCategoryIdByTitle(category.getTitle()));
            category.setId(idCategory);
            return category;
        }else {
            return null;
        }
    }

    @Override
    public Category getCategoryById(int Id) {
        return categoryRespository.getCategoryById(Id);
    }
}
