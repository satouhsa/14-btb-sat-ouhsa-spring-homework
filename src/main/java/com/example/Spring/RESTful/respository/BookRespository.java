package com.example.Spring.RESTful.respository;


//import com.example.Spring.RESTful.pagination.Pagination;
import com.example.Spring.RESTful.respository.dto.Bookdto;
import com.example.Spring.RESTful.respository.dto.Category;
import com.example.Spring.RESTful.respository.provinder.BookProvider;
import com.example.Spring.RESTful.rest.request.BookRestModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRespository {

    @InsertProvider(value = BookProvider.class, method = "insertBook")
    @Results({
            @Result(column = "category_id", property = "category")

    })
    boolean insertBook(Bookdto bookdto);


    @DeleteProvider(value = BookProvider.class, method = "deleteBook")
    boolean deleteBook(int id);

    @SelectProvider(value = BookProvider.class, method = "selectCategoryTitleById")
    String selectCategoryTitleById(int id);

    @UpdateProvider(value = BookProvider.class, method = "updateBook")
    boolean updateBook(int id, Bookdto bookdto);

    //Get All Books
    @SelectProvider(value = BookProvider.class, method = "findAll")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectCatById")),
    })
    List<Bookdto> findAll();

    //Get Category
    @SelectProvider(value = BookProvider.class, method = "selectCatById")
    Category selectCatById(int categoryId);

    @SelectProvider(value = BookProvider.class,method = "findOneById")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectCatById")),

    })

    Bookdto findOneById(int id);

//    @Select("SELECT * FROM tb_categories ORDER BY id ASC LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
//    List<BookRestModel> findAll(@Param("pagination") Pagination pagination);

}




