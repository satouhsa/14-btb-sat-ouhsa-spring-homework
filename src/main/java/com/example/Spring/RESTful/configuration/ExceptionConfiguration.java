package com.example.Spring.RESTful.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Configuration
@ControllerAdvice
public class ExceptionConfiguration extends RuntimeException {


    private static final long serialVersionUID = 2223401815627927627L;

    @JsonProperty("CODE")
    private String code;
    @JsonProperty("MESSAGE")
    private String message;

    public ExceptionConfiguration(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handle(Exception e){
        return new ResponseEntity <>(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
}
