package com.example.Spring.RESTful.service.impl;

import com.example.Spring.RESTful.respository.BookRespository;
import com.example.Spring.RESTful.respository.dto.Bookdto;
import com.example.Spring.RESTful.rest.response.Messages;
import com.example.Spring.RESTful.service.BookService;
import org.apache.logging.log4j.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl  implements BookService {


//    private ArticleRepository articleRepository;
    private BookRespository bookRespository;


    @Autowired
    public void setBookRespository(BookRespository bookRespository) {
//        this.articleRepository = articleRepository;
        this.bookRespository=bookRespository;
    }




    @Override
    public List<Bookdto> findAll() {
        return bookRespository.findAll();
    }

    @Override
    public Bookdto insertBook(Bookdto bookdto) {
        boolean isInserted=bookRespository.insertBook(bookdto);
        if(isInserted){
            String title=bookRespository.selectCategoryTitleById(bookdto.getCategory().getId());
            bookdto.getCategory().setTitle(title);
            return bookdto;
        }else {
            return null;
        }
    }

    @Override
    public String deleteBook(int Id) {
        boolean isDeleted=bookRespository.deleteBook(Id);
        if(isDeleted){
            return Messages.Success.DELETED_SUCCESS.getMessage();
        }else {
            return Messages.Error.DELETED_FAILUR.getMessage();
        }
    }

    @Override
    public Bookdto updateBook(int Id, Bookdto bookdto) {
        boolean isUpdate=bookRespository.updateBook(Id,bookdto);
        if(isUpdate){
            String title=bookRespository.selectCategoryTitleById(bookdto.getCategory().getId());
            bookdto.getCategory().setTitle(title);
            return bookdto;
        }else {
            return null;
        }
    }

    @Override
    public Bookdto findOneById(int Id) {
        return bookRespository.findOneById(Id);
//        Bookdto bookDto=bookRespository.findOneById(Id);
//        if(bookDto!=null){
//            return bookDto;
//        }else {
//            return null;
//        }
    }




}
