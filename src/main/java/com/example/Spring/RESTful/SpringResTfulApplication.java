package com.example.Spring.RESTful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringResTfulApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringResTfulApplication.class, args);
	}

}
