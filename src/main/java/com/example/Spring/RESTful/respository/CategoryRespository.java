package com.example.Spring.RESTful.respository;

import com.example.Spring.RESTful.respository.dto.Bookdto;
import com.example.Spring.RESTful.respository.dto.Category;
import com.example.Spring.RESTful.respository.provinder.BookProvider;
import com.example.Spring.RESTful.respository.provinder.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRespository {

    @SelectProvider(value = CategoryProvider.class, method = "getAllCategories")
    List<Category> getAllCategories();

    @SelectProvider(value = CategoryProvider.class, method = "getCategoryById")
    Category getCategoryById(int id);

    @InsertProvider(value = CategoryProvider.class, method = "postCategory")
    Boolean postCategory(Category category);

    @SelectProvider(value = CategoryProvider.class, method = "getCategoryIdByTitle")
    String getCategoryIdByTitle(String title);

    @UpdateProvider(value = CategoryProvider.class, method = "updateCategory")
    Boolean updateCategory(int id,Category categoryDto);

    @DeleteProvider(value = CategoryProvider.class, method = "deleteCategory")
    Boolean deleteCategory(int id);


}
