package com.example.Spring.RESTful.respository.provinder;

import static org.postgresql.core.SqlCommandType.SELECT;
import static org.springframework.http.HttpHeaders.FROM;

import com.example.Spring.RESTful.respository.dto.Bookdto;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;


public class BookProvider {




        public String insertBook(){
            return new SQL(){{
                INSERT_INTO("book");
                VALUES("title","#{title}");
                VALUES("category_id","#{category.id}");
                VALUES("author","#{author}");
                VALUES("description","#{description}");
                VALUES("thumbnail","#{thumbnail}");
            }}.toString();
        }
        public String deleteBook(){
            return new SQL(){{
                DELETE_FROM("book");
                WHERE("id=#{id}");
            }}.toString();
        }
        public String selectCategoryTitleById(){
            return new SQL(){{
                SELECT("title");
                FROM("categories");
                WHERE("id=#{id}");

            }}.toString();
        }
        public String updateBook(){
            return new SQL(){{
                UPDATE("book");
                SET("title=#{bookdto.title}");
                SET("category_id=#{bookdto.category.id}");
                SET("author=#{bookdto.author}");
                SET("description=#{bookdto.description}");
                SET("thumbnail=#{bookdto.thumbnail}");
                WHERE("id=#{id}");
            }}.toString();
        }

        public String findAll(){
            return new SQL(){{
                SELECT("*");
                FROM("book");
            }}.toString();
        }
        public String selectCatById(){
            return new  SQL(){{
                SELECT("*");
                FROM("categories");
                WHERE("id=#{category_id}");
            }}.toString();
        }
        public String findOneById(){
            return new SQL(){{
                SELECT("*");
                FROM("book");
                WHERE("id=#{id}");
            }}.toString();
        }

    }








